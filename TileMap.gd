extends TileMap

var mapsize : int = 1000
var loops : int = 100
var loop_duration : float
var duration : float = 0
var tile_id : int = 0
var fast_noise = FastNoiseLite.new()
var noise_value : float
var start_time : float
var finish_time : float

func _ready():
   for i in loops:
      fill_tilemap()
      duration = duration + loop_duration
   print("===================")
   print(duration / loops)

func fill_tilemap():
   start_time = Time.get_unix_time_from_system()
   fast_noise.seed = randi_range(0,1000)
   for x in mapsize:
      for y in mapsize:
         noise_value = fast_noise.get_noise_2d(x,y)
         
         if noise_value < 0:
            tile_id = 0
         else:
            tile_id = 1
         set_cell(0,Vector2i(x,y),tile_id,Vector2i.ZERO)
   finish_time = Time.get_unix_time_from_system()
   loop_duration = finish_time - start_time;
   print(loop_duration)
